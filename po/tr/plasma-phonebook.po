# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-phonebook package.
#
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-phonebook\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-05 00:40+0000\n"
"PO-Revision-Date: 2023-05-16 12:10+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Emir SARI"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "emir_sari@icloud.com"

#: src/kpeopleactionplugin/kpeopleactionsplugin.cpp:40
#, kde-format
msgctxt "Action to write to instant messenger contact"
msgid "%1 %2"
msgstr "%1 %2"

#: src/main.cpp:34 src/qml/ContactsPage.qml:21 src/qml/Main.qml:17
#, kde-format
msgid "Phonebook"
msgstr "Telefon Rehberi"

#: src/main.cpp:36
#, kde-format
msgid "View and edit contacts"
msgstr "Kişileri görüntüleyin ve düzenleyin"

#: src/qml/AddContactPage.qml:37
#, kde-format
msgid "Adding contact"
msgstr "Kişi ekleniyor"

#: src/qml/AddContactPage.qml:41
#, kde-format
msgid "Editing contact"
msgstr "Kişi düzenleniyor"

#: src/qml/AddContactPage.qml:51
#, kde-format
msgid "Photo"
msgstr "Fotoğraf"

#: src/qml/AddContactPage.qml:107
#, kde-format
msgid "Add profile picture"
msgstr "Profil resmi ekle"

#: src/qml/AddContactPage.qml:117
#, kde-format
msgid "Name"
msgstr "Ad"

#: src/qml/AddContactPage.qml:143 src/qml/AddContactPage.qml:165
#, kde-format
msgid "Phone"
msgstr "Telefon"

#: src/qml/AddContactPage.qml:167
#, kde-format
msgid "+1 555 2368"
msgstr "+90 212 123 4567"

#: src/qml/AddContactPage.qml:177
#, kde-format
msgid "Add phone number"
msgstr "Telefon numarası ekle"

#: src/qml/AddContactPage.qml:216 src/qml/AddContactPage.qml:238
#, kde-format
msgid "Email"
msgstr "E-posta"

#: src/qml/AddContactPage.qml:240
#, kde-format
msgid "user@example.org"
msgstr "kullanıcı@örnek.org"

#: src/qml/AddContactPage.qml:251
#, kde-format
msgid "Add email address"
msgstr "E-posta adresi ekle"

#: src/qml/AddContactPage.qml:289 src/qml/AddContactPage.qml:303
#, kde-format
msgid "Instant Messenger"
msgstr "Anlık İletici"

#: src/qml/AddContactPage.qml:305
#, kde-format
msgid "protocol:person@example.com"
msgstr "protokol:kişi@örnek.com"

#: src/qml/AddContactPage.qml:324
#, kde-format
msgid "Add instant messenger address"
msgstr "Anlık iletici adresi ekle"

#: src/qml/AddContactPage.qml:343 src/qml/DetailPage.qml:279
#, kde-format
msgid "Birthday"
msgstr "Doğum Günü"

#: src/qml/AddContactPage.qml:362 src/qml/DetailPage.qml:294
#, kde-format
msgid "Note:"
msgstr "Not:"

#: src/qml/ContactsPage.qml:88
#, kde-format
msgid "Import contacts"
msgstr "Kişileri içe aktar"

#: src/qml/ContactsPage.qml:99
#, kde-format
msgid "add Contact"
msgstr "Kişi Ekle"

#: src/qml/ContactsPage.qml:133
#, kde-format
msgid "No contacts"
msgstr "Kişi yok"

#: src/qml/DetailPage.qml:50
#, kde-format
msgid "Edit"
msgstr "Düzenle"

#: src/qml/DetailPage.qml:55
#, kde-format
msgid "Delete contact"
msgstr "Kişi sil"

#: src/qml/DetailPage.qml:66
#, kde-format
msgid "Remove Contact"
msgstr "Kişiyi Kaldır"

#: src/qml/DetailPage.qml:76
#, kde-format
msgid "Are you sure you want to delete <b>%1</b> from your Contacts?"
msgstr "<b>%1</b> kişisini silmek istediğinizden emin misiniz?"

#: src/qml/DetailPage.qml:207
#, kde-format
msgid "Call"
msgstr "Ara"

#: src/qml/DetailPage.qml:214
#, kde-format
msgid "Send SMS"
msgstr "SMS gönder"

#: src/qml/DetailPage.qml:239
#, kde-format
msgid "Send E-Mail"
msgstr "E-posta Gönder"

#: src/qml/DetailPage.qml:245
#, kde-format
msgid "E-Mail"
msgstr "E-posta"

#: src/qml/DetailPage.qml:261
#, kde-format
msgid "Send Message"
msgstr "İleti gönder"

#~ msgid "Create New"
#~ msgstr "Yeni Oluştur"

#~ msgid "Select number to call"
#~ msgstr "Aranacak numarayı seçin"

#~ msgid "Select number to send message to"
#~ msgstr "İleti gönderilecek numarayı seçin"

#~ msgid "Cancel"
#~ msgstr "İptal"
